$(function() {

      function fetchEntries() {
            $.get('/entry', function(data) {
              $('#guestbookEntries').empty();
              $.each(data, function(key, value) {
                  $('#guestbookEntries').append('<div class="list-group-item"> <h4 class="list-group-item-heading">'
                  		+ value.name + '</h4><p class="list-group-item-text">' + value.text + '</p></div>');
              });
            });
       };

      fetchEntries();

      function transformFormToJSON(formValues) {
        let o = {};
        $.each(formValues, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return JSON.stringify(o);
      };

      $('#entryForm').submit(function(event) {
        event.preventDefault();
        let data = $(this).serializeArray();

        $.ajax({
           url: "/entry",
           type: "POST",
           data: transformFormToJSON(data),
           contentType: "application/json;charset=UTF-8",
           success: function() {
            fetchEntries();
            $('#userName, #userText').val('');
            window.location.pathname = '';
           }
        });
      });

});