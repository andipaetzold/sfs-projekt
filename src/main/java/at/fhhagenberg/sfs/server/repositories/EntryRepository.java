package at.fhhagenberg.sfs.server.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import at.fhhagenberg.sfs.server.domain.Entry;

public interface EntryRepository extends PagingAndSortingRepository<Entry, Long> {
}
