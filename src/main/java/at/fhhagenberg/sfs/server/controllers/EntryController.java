package at.fhhagenberg.sfs.server.controllers;

import at.fhhagenberg.sfs.server.domain.Entry;
import at.fhhagenberg.sfs.server.repositories.EntryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("entry")
public class EntryController {
    private final EntryRepository repository;

    @Autowired
    public EntryController(final EntryRepository repository) {
        this.repository = repository;

        final Entry entry1 = new Entry();
        entry1.setName("Jochen");
        entry1.setText("Super toller Beispielkommentar");
        repository.save(entry1);

        final Entry entry2 = new Entry();
        entry2.setName("Rolf");
        entry2.setText("Krasser shit");
        repository.save(entry2);

        final Entry entry3 = new Entry();
        entry3.setName("Dieter");
        entry3.setText("Das is voll krass vong XSS her.");
        repository.save(entry3);

        final Entry entry4 = new Entry();
        entry4.setName("Barbara");
        entry4.setText("Ich bin Barbara.");
        repository.save(entry4);

        final Entry entry5 = new Entry();
        entry5.setName("Scheremie");
        entry5.setText("Oh oh oh, ich bin ein umgedrehter Weihnachtsmann.");
        repository.save(entry5);

        final Entry entry6 = new Entry();
        entry6.setName("Schantal");
        entry6.setText("Das is voll die super duper Website. Bussi, Schanti.");
        repository.save(entry6);
    }

    @GetMapping("")
    public Iterable<Entry> findAll() {
        return repository.findAll();
    }

    @PostMapping("")
    public void create(@RequestBody final Entry entry) {
        repository.save(entry);
    }
}
